# Scanner
<br />
<div align="center">
  <a href="https://gitlab.com/pet-projects26/pcm2mp3.git">
    <img src="images/fingerprint_scan.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Scan 2 markdown</h3>

  <p align="center">
    Simple bash script scan multiple host with predefine ports
    <br />
    <a href="#getting-started"><strong>Explore the docs »</strong></a>

  </p>
</div>

## Description



## Getting Started

### Dependencies

* Bash
* Linux
* Text file contains host
* Text file contains list port

### Installing

* Clone repo
```bash
git clone https://gitlab.com/pet-projects26/scanner.git
```
 
* Change working directory
```bash
cd scanner
```

### Executing program

* Create text file contains all host need to scan.
```bash
# vim hosts.txt
    host 1
    host 2
    host 3
```

* Create text file contains all ports need to scan.
```bash
# vim ports.txt
22,80,443,8444,20000,20001,22001-22105
```
Save and close file

* Add execute permissions to file
```bash
chmod +x scanner.sh
```

* Run script without any arguments to display help message
```bash
./scanner.sh

Usage: scan_reports.sh HOST_FILE PORT_FILE [OPTION] -v -c

Options:
-v --verbose      Verbose
-c --create       Clean up output and create zip file
```

<p align = "center">
<img src = "images/demo.png">
</p>
<p align = "center">
Fig.1 - Sample Output
</p>

## Help

Any advise for common problems or issues please open issue request

## Authors

G
inspired by TamViet

## License

Script is released under the [MIT License](https://opensource.org/licenses/MIT).

## Acknowledgments

Inspiration, code snippets, etc.
* [awesome-readme](https://github.com/matiassingers/awesome-readme)
* [PurpleBooth](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
* [dbader](https://github.com/dbader/readme-template)
* [zenorocha](https://gist.github.com/zenorocha/4526327)
* [fvcproductions](https://gist.github.com/fvcproductions/1bfc2d4aecb01a834b46)
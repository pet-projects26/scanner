#!/bin/bash

cleanUp() {

        rm $(pwd)/scan_result/out1
        rm $(pwd)/scan_result/out2

        zip -rj Reports.zip $(pwd)/scan_result/
        if [ -f "Reports.zip" ]
        then
                echo "------------------------------"
                echo "Cleaning up!!!"
                rm -rf $(pwd)/scan_result

        else
                zip -rj Reports.zip $(pwd)/scan_result/
        fi

}

scanner() {
        SCAN_HOST=${1}
        SCAN_PORTS=${2}
        
        if [ -d "$(pwd)/scan_result" ]
        then
                :
        else
                if [ -z "$3" ]
                then
                        mkdir -p "$(pwd)/scan_result"
                elif
                        mkdir -p "$(pwd)/scan_result"
                        echo "Create result directory"
                fi
        fi

        if [ -z "$3" ]
        then
                :
        else
                echo "------------------------------"
                echo "Scanning $SCAN_HOST"
        fi

        # Start with very fast syn only scan
        nmap -Pn -v -v -sS -p${SCAN_PORTS} -T4 ${SCAN_HOST} > $(pwd)/scan_result/out1

        # Transport open ports into new list for extensive scan
        list=""; 
        for port in $(egrep -i "\s\sopen" $(pwd)/scan_result/out1 | sed -e 's/\/.*//;');
                do list="$list,$port"; 
        done;

        if [ -z "$3" ]
        then
                :
        else
                echo "Founded port ==> $list"
                echo "Performing extensive scan"
        fi

        # Perform extensive scan
        nmap -Pn -v -v -sC $(echo -n $list | sed -e 's/^,/-p/g;') ${SCAN_HOST} > $(pwd)/scan_result/out2

        # Finally create the report
    
        if [ -z "$3" ]
        then
                :
        else
                echo "Export report for ${SCAN_HOST}"
        fi

        cat > $(pwd)/scan_result/report_${SCAN_HOST}.md << EOF
        
# Host ${SCAN_HOST}

* **Date:** $(date "+%Y-%m-%d %H:%M:%S")
* **Scan Host:** $(hostname)
* **Scan source:** $(curl http://ipinfo.io 2>/dev/null | jq '.ip')

### Results

|  Port  |  Status  |  Service  |  Reason  |
|--------|----------|-----------|----------|
$(sed -e '/^#/d; /^[0-9]\+.* open /!d; s/^/|/g; s/$/|/g; s/\s\+/ | /1; s/\s\+/ | /3; s/\s\+/ | /5;' $(pwd)/scan_result/out1)


### Services
|  Port  |  Status  |  Service  |  Reason  |
|--------|----------|-----------|----------|
$(sed -e '1,/^PORT/d; /^Service Info/,$ {d;}; /^\s*$/d; s/^/|/g; s/\s\+/ | /1; s/\s\+/ | /3; s/\s\+/ | /5; s/\s\+/ | /7;' $(pwd)/scan_result/out2)
$(sed -n '/Service Info/p' $(pwd)/scan_result/out1)

EOF
}

readFile () {
        
        

        if [ -z "$1" ] || [ -z "$2" ] 
        then
                echo "Usage: $(basename $0) HOST_FILE PORT_FILE [OPTION] -v -c"
                echo ""
                echo "Options:"
                echo "-v        Verbose"
                echo "-c        Clean up output and create zip file"
                exit 1
        fi
        
        SCAN_PORT=$(cat $2)
        
        while read HOST; 
        do
                # reading each line
                scanner $HOST $SCAN_PORT $3
                
        done < $1

        if [ -z $4 ]
        then
                :
        else
                cleanUP
        fi
}

readFile $1 $2 $3 $4